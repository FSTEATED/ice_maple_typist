#/usr/bin/python3
#-*- coding:UTF-8 -*-

#
# ----------------------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 42):
# <IFRFSX@protonmail.com> wrote this file.  As long as you retain this notice you
# can do whatever you want with this stuff. If we meet some day, and you think
# this stuff is worth it, you can buy me a beer in return.   IFRFSX.
# ----------------------------------------------------------------------------
#

import name

zidian0 = {
    'a':'第一，',
    'b':'第二，',
    'c':'第三，',
    'd':'第四，',
    'fsw':'自由软件',
    'gtsw':'免费软件',
    'prisw':'专有软件',
    'nfsw':'非自由软件',
    'py':'Python',
    'py3':'Python3',
    'IFRFSX':'冰焰火灵X',
    'rj':'若酱',
    'iy':'是',
    'nn':'不是',
    'fn':'非',
    'if':'如果',
    'tgh':'虽然',
    'then':'那么',
    'wld':'世界',
    'comeh':'来这里',
    's1':'的',
    's2':'地',
    's3':'得',
    'spirit1':'精灵',
    'spirit2':'灵魂',
    'this':'这',
    'zai':'在',
    'meow':'喵',
    'cat':'猫',
    'kitty':'幼猫',
    'cute':'可爱',
    '.d':'，',
    '.j':'。',
    '.z':'——',
    'whe1':'那里',
    'whe2':'哪里',
    'that':'那',
    'this':'这',
    'in':'里',
    'ge':'个',
    'lit':'亮',
    'dak':'暗',
    'clr':'色',
    'red':'红',
    'orge':'橙',
    'yel':'黄',
    'gren':'绿',
    'cyan':'青',
    'blue':'蓝',
    'indg':'靛',
    'purp':'紫',
    'whit':'白',
    'blak':'黑',
    'we':'我们',
    'youall':'你们',
    'no':'不',
    'want':'要',
    'by':'由',
    'make':'制作',
    'author':'作者',
    'program':'程序',
    'software':'软件',
    'please':'请',
    'fly':'飞行',
    'sbg':'沙盒游戏',
    'will':'会',
    'hagb':'郭俊余',
    'gitee':'码云托管平台',
    'osc':'开源中国',
    'project':'项目',
    'maple':'枫',
    'leaf':'叶',
    'org':'组织',
    'ice':'冰',
    'fire':'火',
    'water':'水',
    'river1':'河',
    'river2':'江',
    'river3':'溪',
    'grass':'草',
    'flower':'花',
    'tree':'树',
    'CL':'C语言',
    'use':'使用',
    'wow':'哇呜',
    'yeah':'耶',
    'luge':'语言',
    'bow':'弓',
    'arrow':'箭',
    'inputx':'输入法',
    'error':'错误',
    'fault':'故障',
    'close':'关闭',
    'password':'密码',
    '2D':'二维',
    '3D':'三维',
    '4D':'四维',
    'space':'空间',
    'spacet':'时空',
    'gfw':'大型防火墙',
    'grt1':'伟大的',
    'grt2':'杰出的',
    'grt3':'优异的',
    'grt4':'显著的',
    'vpn':'虚拟专用网',
    'vlan':'虚拟局域网',
    'dong':'东',
    'nan':'南',
    'xi':'西',
    'bei':'北',
    'number1':'数字',
    'number2':'号',
    'help':'帮助',
    'helpx':'救命',
    'thx':'谢谢',
    'play':'玩',
    'player1':'玩家',
    'player2':'球员',
    'alth':'虽然',
    'but':'但是',
    'im':'即时聊天',
    'wx':'微信',
    'lantern':'灯笼',
    'lantern-app':'蓝灯',
    'print':'打印'
    }

x=10

icen=''

fl=open('output_Typist.txt', 'a+', encoding='UTF-8')
fl.write(str('============'))
fl.write('\n')
fl.close()

while x==10:
    
    inputx=str(input("输入字符："))
    
    if inputx in zidian0:
        fl=open('output_Typist.txt', 'a+', encoding='UTF-8')
        fl.write(str(zidian0[inputx]))
        fl.close()
    elif '/q' in inputx:
        icen=str(input('请输入您的名字：'))
        name.ice(icen)
        
        break

    elif '/n' in inputx:
        fl=open('output_Typist.txt', 'a+', encoding='UTF-8')
        fl.write('\n')
        fl.close()

    elif '----' in inputx:
        fl=open('output_Typist.txt', 'a+', encoding='UTF-8')
        fl.write('------------\n')
        fl.close()

    else:
        fl=open('output_Typist.txt', 'a+', encoding='UTF-8')
        fl.write(str(inputx))
        fl.close()
