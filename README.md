# 冰枫打字员

作者：冰焰火灵X
组织：冰枫蓝叶自由文化社区团队（QQ群：128242181）

#### 项目介绍
作为临时输入法使用，可以快速将写入的零散英文字符转译为中文词汇。

#### 软件架构
纯python3编写，分为两个部分——主程序用于打字，附带小程序则是用于最后写入打字者的名字。


#### 安装教程

直接运行 IM_Typist.py 即可

#### 使用说明

请将源码打开查看中文词汇和对应的英文字符串，将英文字符串输入以后即可反馈出中文句子。
指令：

+ /n  下一行
+ /q  结束运行，输入自己的署名

如果你输入的字符串超出字典范围，将会直接把英文字符串输出到 txt 文件里面。

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)